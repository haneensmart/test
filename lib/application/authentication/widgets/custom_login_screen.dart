import 'package:flutter/material.dart'; 
import '../../../core/values/values.dart';

class CustomLoginButton extends StatelessWidget {
  final double height;
  final double width;
  final String text;
  final Function onPressed;

  const CustomLoginButton(
      {Key? key,
      required this.height,
      required this.width,
      required this.text,
      required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25),
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [
             AppColors.kBlueBgColor.withOpacity(1.0),
             AppColors.kBabyBlueBgColor.withOpacity(1.0),
             AppColors.kTextColor.withOpacity(1.0),
          ],
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.25),
            spreadRadius: 0,
            blurRadius: 4,
            offset: const Offset(0, -6), // changes position of shadow
          ),
        ],
      ),
      child: MaterialButton(
        onPressed: () => onPressed(),
        child: Text(
          text,
          style: const TextStyle(
            color: Colors.white,
            fontSize: 15,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
