import 'package:flutter/material.dart';
import 'package:structure/application/authentication/screens/login_screen.dart';


import '../../../core/values/values.dart';
import '../../../main.dart';

class CustomMobileNumberOrEmailTextField extends StatelessWidget {
  final IconData icon;
  final String hintText;
  final TextInputType keyboardType;
  final TextEditingController textEditingController;

  const CustomMobileNumberOrEmailTextField({
    Key? key,
    required this.icon,
    required this.hintText,
    required this.keyboardType,
    required this.textEditingController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: (input) {
        if (input != null && input.toString().isNotEmpty) {
         return null; 
        }
        // if ((RegExp('^[0-9]+\$').hasMatch(input!) && input.length == 8)) {
        //   return null;
        // }
        // if (RegExp("^[a-zA-Z0-9._]+\$").hasMatch(input) &&
        //     input.length >= 6) {
        //   return null;
        // }
        return "invalid email or password";
      },
      controller: textEditingController,
      style: const TextStyle(
        color: AppColors.kTextColor,
        fontSize: 10,
      ),
      keyboardType: keyboardType,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white,
        prefixIcon: Icon(
          icon,
          color: AppColors.kTextColor,
        ),
        hintText: hintText,
        hintStyle: const TextStyle(
          fontSize: 12,
          color: Colors.grey,
        ),
        contentPadding: const EdgeInsets.symmetric(vertical: 5),
      ),
    );
  }
}

class MyWidget extends StatefulWidget {
  const MyWidget({super.key});

  @override
  State<MyWidget> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<MyWidget> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(),
      drawer: NavigationDrawer( 
        onDestinationSelected: (value) {
          setState(() {
            b = value;
          });
          
        },
        selectedIndex: b,
        children: a)
      
      ,body:c[b] ,
      );
      
  }
}
int b = 0 ;
List<Widget> a =[
 Container(
  child: const Text('go to login'),
 ),
];
List<Widget> c =[
 const LoginScreen(),
];
