import 'package:flutter/material.dart';
import 'package:structure/main.dart';

import '../../../core/values/values.dart';


class CustomPasswordTextField extends StatefulWidget {
  final String hintText;
  final TextInputType keyboardType;
  final TextEditingController textEditingController;

  const CustomPasswordTextField(
      {Key? key,
      required this.hintText,
      required this.keyboardType,
      required this.textEditingController})
      : super(key: key);

  @override
  State<CustomPasswordTextField> createState() =>
      _CustomPasswordTextFieldState();
}

class _CustomPasswordTextFieldState extends State<CustomPasswordTextField> {
  bool hiddenPassword = true;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: (input) => ((input!.length) < 6)
          ? "invalid password"
          : null,
      controller: widget.textEditingController,
      style: const TextStyle(
        color: AppColors.kTextColor,
        fontSize: 10,
      ),
      obscureText: (hiddenPassword) ? true : false,
      keyboardType: widget.keyboardType,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white,
        prefixIcon: GestureDetector(
          onTap: () {
            setState(() {
              hiddenPassword = !hiddenPassword;
            });
          },
          child: Icon(
            (hiddenPassword)
                ? Icons.visibility_off_outlined
                : Icons.visibility_outlined,
            color: AppColors.kTextColor,
          ),
        ),
        hintText: widget.hintText,
        hintStyle: const TextStyle(
          color: Colors.grey,
          fontSize: 12,
        ),
        contentPadding: const EdgeInsets.symmetric(vertical: 5),
      ),
    );
  }
}
