import 'dart:core';

import 'package:bloc/bloc.dart';
import 'package:nb_utils/nb_utils.dart';

import '../../../../configure_di.dart';
import '../../../../core/data/local_data/local_data_source.dart';
import '../../../../core/values/values.dart';
import '../../../../main.dart';
import '../../models/auth_model.dart';
import '../../repositories/auth_repo.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc(
  ) : super(AuthInitial()) { 

bool validateNameOrPhoneNumber(String name, String phoneNumber) {
  if ((name.isEmpty) && (phoneNumber.isEmpty)) {
    return false;
  }
  
  return true;
}

    on<PostLoginEvent>((event, state) async{

       emit(AuthLoading());
        try { 
         final response = await getIt<AuthRepo>().login(
          event.username, event.password,
         );
      emit(AuthSuccess(response: response!));
      
       } catch (e) { 
         emit(AuthFailure(message: e.toString()));
       } 
   });
  

  
  } 
}


