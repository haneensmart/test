part of 'auth_bloc.dart';

abstract class AuthEvent {} 

class PostLoginEvent extends AuthEvent {
    String username;
    String password;
  PostLoginEvent({
    required this.username,
    required this.password,
  });

  }

