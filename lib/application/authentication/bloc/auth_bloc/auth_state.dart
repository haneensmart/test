part of 'auth_bloc.dart';

abstract class AuthState{

}

class AuthInitial extends AuthState {
 
}

class AuthLoading extends AuthState {

}

class AuthSuccess extends AuthState {
AuthModel response;
  AuthSuccess({
    required this.response,
  }); 
}

class AuthFailure extends AuthState {
 String message;
  AuthFailure({
    required this.message,
  }); 
}
