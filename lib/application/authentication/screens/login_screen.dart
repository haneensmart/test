import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:structure/application/home/screens/main_screen.dart';
import 'package:structure/application/home/screens/products_screen.dart';
import 'package:structure/configure_di.dart';
import 'package:structure/core/data/remote_data/network_client_http.dart';
import 'package:structure/core/utils/extensions.dart';
import 'package:structure/core/values/values.dart';
import '../../../core/app_store/app_store.dart';
import '../../../core/layout/responsive_widget_builders.dart';
import '../bloc/auth_bloc/auth_bloc.dart';
import '../repositories/auth_repo.dart';
import '../widgets/custom_login_screen.dart';
import '../widgets/custom_mobile_number_or_email_field.dart';
import '../widgets/custom_password_text_field.dart';


class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController textEditingPassword = TextEditingController();

  TextEditingController textEditingMobileNumberOrEmail =
      TextEditingController();

  GlobalKey<FormState> loginFormKey = GlobalKey();

  bool formValidate() {
    return loginFormKey.currentState!.validate();
  }

  bool isEnglish = getIt<AppStore>().selectedLanguageCode == "en";
  bool isCollapsed = false;

  @override
  Widget build(BuildContext context) {
    return ResponsiveWidgetBuilder.builder(
      mobile: (context) {

        return BlocProvider(
          create: (context) => AuthBloc(),
          child: BlocListener<AuthBloc, AuthState>(
            listener: (context, state) {
              if (state is AuthSuccess) {
                if (state.response.token != "") {
                  MainScreen().launch(context, isNewTask: true);
                }
              } else if (state is AuthFailure) {
                toast(state.message,
                    bgColor: AppColors.kBabyBlueBgColor,
                    textColor: AppColors.kTextColor);
              }
            },
            child: OrientationLayoutBuilder(
              portrait: (context) => Scaffold(
                  body: Column(
                    children: [
                      Expanded(
                        child: Container(
                          // width: MediaQuery.of(context).size.width,
                          // height: MediaQuery.of(context).size.height/2,
                         decoration:  const BoxDecoration(
                        gradient: LinearGradient(
                          colors: [AppColors.kBabyBlueBgColor, AppColors.kBlueBgColor],
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                        ),
                    ),
                          child: Center(
                            child:Image.asset('assets/images/nano_health.png') ,
                          )  ,
                    ),
                      ),
                      Expanded(
                          child: SingleChildScrollView(
                        physics: const BouncingScrollPhysics(
                            decelerationRate: ScrollDecelerationRate.fast),
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 30.w, vertical: 20),
                              width: 370.w,
                              child: Form(
                                key: loginFormKey,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    CustomMobileNumberOrEmailTextField(
                                      textEditingController:
                                      textEditingMobileNumberOrEmail,
                                      icon: Icons.phone_android,
                                      hintText: "email",
                                      keyboardType: TextInputType.text,
                                    ),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    CustomPasswordTextField(
                                      textEditingController: textEditingPassword,
                                      hintText: "password",
                                      keyboardType: TextInputType.text,
                                    ),
                                    const SizedBox(
                                      height: 50,
                                    ),

                                    BlocBuilder<AuthBloc, AuthState>(
                                      builder: (context, state) {
                                        if (state is AuthLoading) {
                                          return const CustomLoader();
                                        }
                                        return CustomLoginButton(
                                          height: 50.h,
                                          width: 300.w,
                                          text: "Continue",
                                          onPressed: () async {
                                            FocusManager.instance.primaryFocus
                                                ?.unfocus();
                                            if (formValidate()) {
                                              BlocProvider.of<AuthBloc>(context)
                                                  .add(PostLoginEvent(
                                                username:
                                                textEditingMobileNumberOrEmail
                                                    .text
                                                    .trim(),
                                                password:
                                                textEditingPassword.text.trim(),
                                              ));
                                            }
                                          },
                                        );
                                      },
                                    ),
                                    SizedBox(
                                      height: 30.h,
                                    ),
                                    const Center(
                                      child: Text("NEED HELP ?",
                                          style:  TextStyle(
                                            color: Colors.blueGrey,
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold,
                                          )

                                      ),
                                    ),

                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20.h,
                            ),
                          ],
                        ),

                      )),
                    ],
                  )),
              landscape: (context) {
                return Scaffold(
                    body: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: Container(
                            // width: MediaQuery.of(context).size.width,
                            // height: MediaQuery.of(context).size.height/2,
                            decoration:  const BoxDecoration(
                              gradient: LinearGradient(
                                colors: [AppColors.kBabyBlueBgColor, AppColors.kBlueBgColor],
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                              ),
                            ),
                            child: Center(
                              child:Image.asset('assets/images/nano_health.png') ,
                            )  ,
                          ),
                        ),
                        SizedBox(
                          width: 20.h,
                        ),
                        Expanded(
                            child: SingleChildScrollView(
                              physics: const BouncingScrollPhysics(
                                  decelerationRate: ScrollDecelerationRate.fast),
                              child: Column(
                                children: [
                                  Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 30.w, vertical: 20),
                                    width: 370.w,
                                    child: Form(
                                      key: loginFormKey,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          CustomMobileNumberOrEmailTextField(
                                            textEditingController:
                                            textEditingMobileNumberOrEmail,
                                            icon: Icons.phone_android,
                                            hintText: "email",
                                            keyboardType: TextInputType.text,
                                          ),
                                          const SizedBox(
                                            height: 20,
                                          ),
                                          CustomPasswordTextField(
                                            textEditingController: textEditingPassword,
                                            hintText: "password",
                                            keyboardType: TextInputType.text,
                                          ),
                                          const SizedBox(
                                            height: 50,
                                          ),

                                          BlocBuilder<AuthBloc, AuthState>(
                                            builder: (context, state) {
                                              if (state is AuthLoading) {
                                                return const CustomLoader();
                                              }
                                              return CustomLoginButton(
                                                height: 50.h,
                                                width: 300.w,
                                                text: "Continue",
                                                onPressed: () async {
                                                  FocusManager.instance.primaryFocus
                                                      ?.unfocus();
                                                  if (formValidate()) {
                                                    BlocProvider.of<AuthBloc>(context)
                                                        .add(PostLoginEvent(
                                                      username:
                                                      textEditingMobileNumberOrEmail
                                                          .text
                                                          .trim(),
                                                      password:
                                                      textEditingPassword.text.trim(),
                                                    ));
                                                  }
                                                },
                                              );
                                            },
                                          ),
                                          SizedBox(
                                            height: 30.h,
                                          ),
                                          const Center(
                                            child: Text("NEED HELP ?",
                                                style:  TextStyle(
                                                  color: Colors.blueGrey,
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.bold,
                                                )

                                            ),
                                          ),

                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20.h,
                                  ),
                                ],
                              ),

                            ))
                      ],
                    ));
              },
            ),
          ),
        );
      },
    );
  }
}

class CustomLoader extends StatelessWidget {
  const CustomLoader({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: CircularProgressIndicator(color: AppColors.kTextColor),
    );
  }
}
