import 'dart:async';

import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:structure/application/home/screens/main_screen.dart';
import 'package:structure/application/home/screens/products_screen.dart';

import '../../../configure_di.dart';
import '../../../core/data/local_data/local_data_source.dart';
import '../../../core/values/constant.dart';
import '../../../core/values/values.dart';
import 'login_screen.dart';



class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Alignment myAlign1 = Alignment.center;
 // final PusherBeams _beamsClient = PusherBeams.instance;

  @override
  void initState() {
    super.initState();
    checkToken();
  } 
   checkToken() { 
     Timer(const Duration(seconds: 1), ()async {
      String? token = await getIt<LocalDataSource>().get(TOKEN);
      if (token == "" || token == null) {
         return const LoginScreen().launch(context , isNewTask:  true);
      }else {
        return  MainScreen().launch(context , isNewTask:  true);
      }

     });
   }

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      body: Builder(
        builder: (context) {
          return Container(
            decoration:  BoxDecoration(
              gradient: LinearGradient(
                colors: [AppColors.kBabyBlueBgColor, AppColors.kBlueBgColor],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
              ),
            ),
            child: const Center(
              child: SizedBox(
                width: 150,
                height: 158,
                child: Image(
                  image: AssetImage(
                    'assets/images/nano_health.png',
                  ),
                ),
              ),
            ), 
          );
        },
      ),
    );
  }
} 