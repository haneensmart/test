import 'dart:convert';

import 'package:http/http.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:structure/core/data/remote_data/network_client_http.dart';

import '../../../configure_di.dart';
import '../../../core/data/local_data/local_data_source.dart';
import '../../../core/values/constant.dart';
import '../../../core/values/values.dart';
import '../models/auth_model.dart';

abstract class AuthRepo {
  Future<AuthModel?> login(  String password,  String userName);

}
class AuthRepoImp implements AuthRepo {
   final NetworkClient networkClient;

   AuthRepoImp(this.networkClient);

  @override
  Future<AuthModel?> login(String userName, String password) async{
    Response response = await networkClient
          .buildHttpResponse('auth/login', method: HttpMethod.POST, body: {
        "username": userName,
        "password": password,
      });

      Map<String, dynamic> res = (await networkClient.handleResponse(response)) ;
      if (res is Map) {
        if (response.statusCode == 200) {
          final token = json.decode(response.body)['token'];
          getIt<LocalDataSource>().cacheValue(TOKEN, token!);

        }
      }
      return AuthModel.fromJson(res);
  }



}
