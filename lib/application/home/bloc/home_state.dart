part of 'home_bloc.dart';

class HomeState {
  List<Product>? products;
  bool? isLoading;

  HomeState({this.products, this.isLoading});

  factory HomeState.initial() => HomeState(isLoading: false);

  HomeState copyWith({
    List<Product>? products,
    bool? isLoading,
  }) {
    return HomeState(
      isLoading: isLoading ?? this.isLoading,
      products: products ?? this.products,
    );
  }
}
