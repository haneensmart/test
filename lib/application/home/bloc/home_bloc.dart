import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:structure/configure_di.dart';

import '../models/product_model.dart';
import '../repositories/home_repository.dart';
part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(HomeState.initial()) {

    on<ProductEvent>((event, emit) async {
      emit(state.copyWith(isLoading: true));

      await getIt<HomeRepository>().getCategoriesApi().then((value) {
        emit(state.copyWith(products: value, isLoading: false));
      }).catchError((e) {
        toast('$e', print: true, bgColor: Colors.teal);
      });
    });

    add(ProductEvent());

  }
}
