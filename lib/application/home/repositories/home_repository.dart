
import 'package:structure/core/data/remote_data/network_client_http.dart';
import '../models/product_model.dart';

abstract class HomeRepository {
  Future<List<Product>?> getCategoriesApi();
}

class HomeRepositoryImpl implements HomeRepository {
  final NetworkClient networkClient;

  const HomeRepositoryImpl(this.networkClient);

  @override
  Future<List<Product>?> getCategoriesApi() async {
    Iterable res = (await networkClient.handleResponse(
        await networkClient.buildHttpResponse("products",
            method: HttpMethod.GET)));
    return res.map((e) => Product.fromJson(e)).toList();
  }


}
