import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:structure/application/details/screens/product_details_screen.dart';
import 'package:structure/application/home/bloc/home_bloc.dart';

import '../../../core/values/values.dart';
import '../widgets/product_widget.dart';

class ProductsScreen extends StatefulWidget {
  ProductsScreen({Key? key}) : super(key: key);

  @override
  State<ProductsScreen> createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> {
  TextEditingController textFieldController = TextEditingController();

  // Widget currentTab = ProductsScreen();
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomeBloc(),
      child: Scaffold(
        body: BlocConsumer<HomeBloc, HomeState>(
          listener: (context, state) {},
          builder: (context, state) {
            if (state.isLoading!) {
              return const CircularProgressIndicator(
                color: AppColors.kTextColor,
              ).center();
            } else {
              return (state.products == null || state.products!.isEmpty)
                  ? const Text('no data').center()
                  : ListView.separated(
                      itemBuilder: (context, index) =>
                          InkWell(
                              onTap:() {
                                Navigator.of(context, rootNavigator: true)
                                    .push(
                                  MaterialPageRoute(
                                    builder: (context) =>
                                        ProductDetailsScreen(
                                          productId: state.products![index].id!,
                                        ),
                                  ),
                                );

                              },
                              child: ProductWidget(product: state.products![index])),
                      separatorBuilder: (context, index) => const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 8.0),
                        child: Divider(
                          thickness: 2,
                          color: Colors.grey,

                        ),
                      ),
                      itemCount: state.products!.length);
            }
          },
        ),
      ),
    );
  }
}
