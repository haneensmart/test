import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:structure/application/authentication/screens/login_screen.dart';
import 'package:structure/application/home/screens/products_screen.dart';

// ignore: must_be_immutable
class MainScreen extends StatefulWidget {
  const MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight:MediaQuery.of(context).size.height/9,
        title: Center(child: Text('All PRODUCTS', style:  TextStyle(
          color: Colors.blueGrey,
          fontSize: 15,
          fontWeight: FontWeight.bold,
        ))),
        backgroundColor: Colors.white54,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(40),
          ),
        ),
      ),
      bottomNavigationBar: ClipRRect(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(25),
          topLeft: Radius.circular(25),
        ),
        child: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        showUnselectedLabels: true,
        backgroundColor:Colors.white,
        selectedFontSize: 10,
        unselectedFontSize: 10,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Container(
              decoration: BoxDecoration(
                color: Colors.transparent,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              child: CircleAvatar(
                radius: 25,
                backgroundColor:
                (_selectedIndex == 0) ? Colors.white : Colors.transparent,
                child: Image(
                  image: AssetImage("assets/images/Home.png"),
                ),
              ),
            ),
            label: 'Menu',
          ),
          BottomNavigationBarItem(
            icon: Container(
              decoration: BoxDecoration(
                color: Colors.transparent,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              child: CircleAvatar(
                radius: 25,
                backgroundColor:
                (_selectedIndex == 1) ? Colors.white : Colors.transparent,
                child: Image(
                  image: AssetImage("assets/images/Cart.png"),
                ),
              ),
            ),
            label:  'Cart',
          ),
          BottomNavigationBarItem(
            icon: Container(
              decoration: BoxDecoration(
                color: Colors.transparent,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              child: CircleAvatar(
                radius: 25,
                backgroundColor:
                (_selectedIndex == 2) ? Colors.white : Colors.transparent,
                child: Image(
                  image: AssetImage("assets/images/Like.png"),
                ),
              ),
            ),
            label: 'Favourite',
          ),
          BottomNavigationBarItem(
            icon: Container(
              decoration: BoxDecoration(
                color: Colors.transparent,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              child: CircleAvatar(
                radius: 25,
                backgroundColor:
                (_selectedIndex == 3) ? Colors.white : Colors.transparent,
                child: Image(
                  image: AssetImage("assets/images/User.png"),
                ),
              ),
            ),
            label: 'Profile',
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
    ),
      ),

      body: ProductsScreen(),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
     // widget.toDashboardScreen = false;
      _selectedIndex = index;
    });
  }
}
