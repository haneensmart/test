
import 'package:structure/core/data/remote_data/network_client_http.dart';

import '../../home/models/product_model.dart';


abstract class ProductDetailsRepository {
  Future<Product?> getProductDetailsApi(int productId);
}

class ProductDetailsRepositoryImpl implements ProductDetailsRepository {
  final NetworkClient networkClient;

  const ProductDetailsRepositoryImpl(this.networkClient);

  @override
  Future<Product?> getProductDetailsApi(int productId) async {
    return Product.fromJson((await networkClient.handleResponse(
        await networkClient.buildHttpResponse("products/$productId",
            method: HttpMethod.GET,
            ))));
  }


}
