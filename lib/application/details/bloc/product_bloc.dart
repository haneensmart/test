import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:structure/configure_di.dart';

import '../../home/models/product_model.dart';
import '../repositories/product_repository.dart';
part 'product_event.dart';
part 'product_state.dart';

class ProductDetailsBloc extends Bloc<ProductDetailsEvent, ProductDetailsState> {
  final int productId;
  ProductDetailsBloc(this.productId) : super(ProductDetailsState.initial()) {

    on<ProductDetailsEvent>((event, emit) async {
      emit(state.copyWith(isLoading: true));

      await getIt<ProductDetailsRepository>().getProductDetailsApi(event.productId).then((value) {
        emit(state.copyWith(product: value, isLoading: false));
      }).catchError((e) {
        toast('$e', print: true, bgColor: Colors.teal);
      });
    });

    add(ProductDetailsEvent(productId:productId));

  }
}
