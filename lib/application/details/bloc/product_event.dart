part of 'product_bloc.dart';

abstract class ProductEvent {}

class ProductDetailsEvent extends ProductEvent {
  int productId;
  ProductDetailsEvent( {required this.productId});
}



