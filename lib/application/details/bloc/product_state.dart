part of 'product_bloc.dart';

class ProductDetailsState {
  Product? product;
  bool? isLoading;

  ProductDetailsState({this.product, this.isLoading});

  factory ProductDetailsState.initial() => ProductDetailsState(isLoading: false);

  ProductDetailsState copyWith({
    bool? isLoading, Product? product,
  }) {
    return ProductDetailsState(
      isLoading: isLoading ?? this.isLoading,
      product: product ?? this.product,
    );
  }
}
