part of values;

class AppColors {
  static const Color kBabyBlueBgColor = Color(0xFF2AB3C6);
  static const Color kBlueBgColor = Color(0xFF188095);
  static const Color kTextColor = Color(0xFF08293B);
}
